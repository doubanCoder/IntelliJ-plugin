# 此项目只用于开发Intellij IDEA插件
##需求相关信息
###API 文档：
[ http://git.oschina.net/GitGroup/extension-docs/blob/master/API/RequiredAPI.md](http://git.oschina.net/GitGroup/extension-docs/blob/master/API/RequiredAPI.md)
###IntelliJ IDEA 文档：
[http://git.oschina.net/GitGroup/extension-docs/blob/master/IntelliJ-IDEA/README.md](http://git.oschina.net/GitGroup/extension-docs/blob/master/IntelliJ-IDEA/README.md)
## 开发环境搭建
[Setting Up a Development Environment](http://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/setting_up_environment.html)
## 运行/调试插件
[Running and Debugging a Plugin](http://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/running_and_debugging_a_plugin.html)
## 部署插件
[Deploying a Plugin](http://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/deploying_plugin.html)
## 发布插件
[Publishing a plugin](http://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/publishing_plugin.html)
